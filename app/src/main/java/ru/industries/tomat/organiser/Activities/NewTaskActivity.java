package ru.industries.tomat.organiser.Activities;

import android.app.DatePickerDialog;
import android.support.v4.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.SelectableAdapter;
import eu.davidea.flexibleadapter.utils.Utils;
import eu.davidea.viewholders.FlexibleViewHolder ;
import ru.industries.tomat.organiser.AbstractTypeItem;
import ru.industries.tomat.organiser.DBHelper;
import ru.industries.tomat.organiser.Fragments.SubTypeDialogFragment;
import ru.industries.tomat.organiser.R;
import ru.industries.tomat.organiser.TaskTable;
import ru.industries.tomat.organiser.TypeTable;

public class NewTaskActivity extends AppCompatActivity implements FlexibleAdapter.OnItemClickListener, SubTypeDialogFragment.SubTypeDialogListener{

    private EditText ETName;
    private TextView DateText;
    private TextView TimeText;
    private TextView selType;
    private EditText ETDescription;
    private Spinner SpinTime;
    private Spinner SpinParent;
    private int Gid = -1;
    private int TaskId = -1;
    FlexibleAdapter mAdapter = null;
    private List<TypeItem> mItems;


    private Calendar CurDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.new_task);

        //get CurDate
        CurDate = Calendar.getInstance();

        //get Edit texts
        ETName = (EditText) findViewById(R.id.editText_Name) ;
        ETDescription = (EditText) findViewById(R.id.editText_Desc);

        //get Text View
        selType = (TextView) findViewById(R.id.sel_type_text);
        DateText = (TextView) findViewById(R.id.textDate);
        DateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateDialog();
            }
        });
        TimeText = (TextView) findViewById(R.id.textTime);
        TimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeDialog();
            }
        });
        updDateTime();

        //get type list
        List<TypeTable> TT = DBHelper.getTypeTableByParentId(-1);

        //FlexibleAdapter

        mItems =  new ArrayList<TypeItem>();

        for (int i = 0; i < TT.size(); i++) {

            TypeItem typeItem = new TypeItem(String.valueOf(TT.get(i).getId()));
            typeItem.setTitle(TT.get(i).getName());
            typeItem.setColor(TT.get(i).getColor());
            mItems.add(typeItem);

        }

        mAdapter = new FlexibleAdapter(mItems, this);
        mAdapter.setMode(SelectableAdapter.MODE_SINGLE);

        final RecyclerView RView = (RecyclerView) findViewById(R.id.TypeListView);
        RView.setLayoutManager(new LinearLayoutManager(NewTaskActivity.this, LinearLayoutManager.HORIZONTAL, false));

        RView.setAdapter(mAdapter);// set adapter on recyclerview
        RView.setHasFixedSize(true);
        RView.setItemAnimator(new DefaultItemAnimator() {
            @Override
            public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
                //NOTE: This allows to receive Payload objects on notifyItemChanged called by the Adapter!!!
                return true;
            }
        });


        mAdapter.notifyDataSetChanged();// Notify the adapter



        String[] data = {"Не уведомлять", "two", "three", "four", "five"};
        SpinTime = (Spinner) findViewById(R.id.spin_time);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SpinTime.setAdapter(adapter);
        SpinTime.setSelection(0);

        //TODO Заполнить временем


        int sel = -1;
        boolean hasChilds = false;
        Bundle extras = getIntent().getExtras();
        if(extras!=null) {
            TaskId = extras.getInt("TaskID");
            sel = DBHelper.getTaskById(TaskId).getParentId();
            if(DBHelper.getTaskTableFilterByParentId(TaskId).size()>0) {
                hasChilds = true;
            }
        }

        List<TaskTable> tasks = DBHelper.getTaskTableFilterByParentId(-1);
        List<TaskItem> mTasks = new ArrayList<TaskItem>();
        mTasks.add(new TaskItem(String.valueOf(-1), "Главная задача", "", 0, ""));

        if(!hasChilds) {

            for (int i = 0; i < tasks.size(); i++) {

                TaskTable task = tasks.get(i);

                if (task.getId() == sel) {
                    sel = i;
                }

                TypeTable type = DBHelper.getTypeById(task.getTypeId());
                int color = type.getColor();
                String badget = "";
                if(type.getParentId()>=0){
                    TypeTable parent = DBHelper.getTypeById(type.getParentId());
                    color = parent.getColor();
                    badget = String.valueOf(parent.getName().charAt(0));
                }
                mTasks.add(new TaskItem(String.valueOf(task.getId()), task.getName(), "", color, badget));
            }
        }

        SpinParent = (Spinner) findViewById(R.id.spin_parent_task);
        SpinParent.setAdapter(new MyTaskSpinnerAdapter(this, mTasks));
        SpinParent.setSelection(sel+1);
        SpinParent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    RView.setVisibility(View.VISIBLE);
                    selType.setVisibility(View.VISIBLE);
                    Gid = -1;
                }
                else {
                    RView.setVisibility(View.GONE);
                    selType.setVisibility(View.GONE);
                    TaskItem item = (TaskItem)SpinParent.getSelectedItem();
                    Gid =  DBHelper.getTaskById(Integer.parseInt(item.id)).getTypeId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        loadContent();
    }

    private void loadContent(){

        if(TaskId>=0){
            TaskTable task = DBHelper.getTaskById(TaskId);
            ETName.setText(task.getName());
            CurDate.setTimeInMillis(task.getDateMili());
            updDateTime();

            Gid = task.getTypeId();

            ETDescription.setText(task.getDescription());

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.button_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.save_item:
                if (checkInfo()) {
                    createTask();
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                } else
                    Toast.makeText(NewTaskActivity.this, "Необходимые данные не введены!", Toast.LENGTH_SHORT).show();
                return true;
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updDateTime()
    {
        DateText.setText(MainActivity.dateForm.format(CurDate.getTime()));
        TimeText.setText(MainActivity.timeForm.format(CurDate.getTime()));
    }

    private boolean checkInfo()
    {
        //TODO More checks
        if(!ETName.getText().toString().isEmpty()&&Gid != -1) {
            return true;
        }
        return false;
    }

    private void createTask()
    {
        TaskItem item = (TaskItem) SpinParent.getSelectedItem();
        if(TaskId>=0){
            DBHelper.updateTask(TaskId, ETName.getText().toString(), CurDate.getTimeInMillis(), Gid, false, 0, ETDescription.getText().toString(), Integer.parseInt(item.id));
        }
        else {
            DBHelper.addTask(ETName.getText().toString(), CurDate.getTimeInMillis(), Gid, false, 0, ETDescription.getText().toString(), Integer.parseInt(item.id));
        }
    }

    private void dateDialog()
    {
        new DatePickerDialog(this, DateCallBack, CurDate.get(Calendar.YEAR), CurDate.get(Calendar.MONTH), CurDate.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener DateCallBack = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            CurDate.set(Calendar.YEAR, year);
            CurDate.set(Calendar.MONTH, monthOfYear);
            CurDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updDateTime();
        }
    };

    private void timeDialog()
    {
        new TimePickerDialog(this, TimeCallBack, CurDate.get(Calendar.HOUR_OF_DAY), CurDate.get(Calendar.MINUTE), DateFormat.is24HourFormat(this)).show();
    }

    TimePickerDialog.OnTimeSetListener TimeCallBack = new TimePickerDialog.OnTimeSetListener() {

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            CurDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
            CurDate.set(Calendar.MINUTE, minute);
            updDateTime();
        }
    };

    public void showSubTypeDialog(int parentID) {

        if(DBHelper.getTypeTableByParentId(parentID).size()>0) {

            DialogFragment dialog = new SubTypeDialogFragment();

            Bundle args = new Bundle();
            args.putInt("ParentID", parentID);
            dialog.setArguments(args);

            dialog.show(getSupportFragmentManager(), "subtype");
        }
    }

    @Override
    public boolean onItemClick(int position) {

        TypeItem typeItem = mItems.get(position);

        Gid = Integer.parseInt(typeItem.getId());
        selType.setText(typeItem.getTitle());

        showSubTypeDialog(Gid);
        //диалог выбора подтипа

        return true;
    }

    @Override
    public void onDialogPositiveClick(int subtaskId) {
        //обновить надпись
        TypeTable main = DBHelper.getTypeById(Gid);
        TypeTable sub = DBHelper.getTypeById(subtaskId);

        String text = main.getName()+", "+sub.getName();

        selType.setText(text);
        Gid = subtaskId;
    }


    class TypeItem extends AbstractTypeItem<ParentViewHolder> {

        public TypeItem(String id) {
            super(id);
        }

        @Override
        public int getLayoutRes() {
            return R.layout.type_list_item;
        }

        @Override
        public ParentViewHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {
            return new ParentViewHolder(inflater.inflate(getLayoutRes(), parent, false), adapter);
        }

        @Override
        @SuppressWarnings("unchecked")
        public void bindViewHolder(final FlexibleAdapter adapter, ParentViewHolder holder, int position, List payloads) {

            holder.title.setText(getTitle());

            //This "if-else" is just an example of what you can do with item animation
            if (adapter.isSelected(position)) {

                holder.imageview.setImageDrawable(TextDrawable.builder().buildRound(" ", 0xff616161));
                holder.checkIcon.setVisibility(View.VISIBLE);

            } else {

                holder.imageview.setImageDrawable(TextDrawable.builder().buildRound(" ", getColor()));
                holder.checkIcon.setVisibility(View.GONE);

            }
        }
    }

    static final class ParentViewHolder extends FlexibleViewHolder {

        public TextView title;
        public ImageView imageview;
        public ImageView checkIcon;
        public View view;
        public Context mContext;

        public ParentViewHolder(View view, FlexibleAdapter adapter) {
            super(view, adapter);
            this.mContext = view.getContext();
            this.view = view;
            this.title = (TextView) view.findViewById(R.id.type_list_title);
            this.imageview = (ImageView) view.findViewById(R.id.type_list_icon);
            this.checkIcon = (ImageView) view.findViewById(R.id.type_list_check_icon);

            this.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mAdapter.mItemClickListener.onItemClick(getAdapterPosition());

                    mAdapter.toggleSelection(getAdapterPosition());
                    toggleActivation();

                }
            });
        }

        @Override
        public void onClick(View view) {
            super.onClick(view);
        }

        @Override
        public boolean onLongClick(View view) {
            return super.onLongClick(view);
        }

        @Override
        protected void toggleActivation() {
            super.toggleActivation();
            //Here we use a custom Animation inside the ItemView
            imageview.setImageDrawable(TextDrawable.builder().buildRound(" ", 0xff616161));
            checkIcon.setVisibility(View.VISIBLE);

        }

        @Override
        public float getActivationElevation() {
            return 0;//Utils.dpToPx(itemView.getContext(), 0f);
        }
    }

    public class MyTaskSpinnerAdapter extends ArrayAdapter<TaskItem> {

        public MyTaskSpinnerAdapter(Context context, List<TaskItem> tasks) {
            super(context, android.R.layout.simple_list_item_2, tasks);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        // This funtion called for each row ( Called data.size() times )
        public View getCustomView(int position, View convertView, ViewGroup parent) {

            TaskItem task = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.fragment_task_item, null);
            }

            ((TextView) convertView.findViewById(R.id.task_item_title)).setText(task.title);
            convertView.findViewById(R.id.task_item_date).setVisibility(View.GONE);
            ((ImageView) convertView.findViewById(R.id.task_item_icon)).setImageDrawable(TextDrawable.builder().buildRound("", task.color));

            return convertView;

        }
    }

    public static class TaskItem {
        public final String id;
        public final String title;
        public final String date_time;
        public final int color;
        public final String badget;

        public TaskItem(String id, String title, String date_time, int color, String badget) {
            this.id = id;
            this.title = title;
            this.date_time = date_time;
            this.color = color;
            this.badget = badget;
        }

        @Override
        public String toString() {
            return title;
        }
    }


}
