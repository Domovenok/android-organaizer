package ru.industries.tomat.organiser.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.larswerkman.holocolorpicker.ColorPicker;

import java.util.List;

import ru.industries.tomat.organiser.DBHelper;
import ru.industries.tomat.organiser.R;
import ru.industries.tomat.organiser.TypeTable;

public class TypeDialogFragment extends DialogFragment implements
        DialogInterface.OnClickListener {

    private View form=null;
    private EditText title=null;
    private ColorPicker picker;
    private Spinner parentSpinner;
    private int id;
    private List<TypeTable> listTypes;

    public interface TypeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    TypeDialogListener mListener;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        form= getActivity().getLayoutInflater()
                .inflate(R.layout.newtypedialog, null);
        picker = (ColorPicker) form.findViewById(R.id.picker);
        picker.setShowOldCenterColor(false);
        title = (EditText) form.findViewById(R.id.typeName);

        listTypes = DBHelper.getTypeTableByParentId(-1);

        int parentId = -1;

        id = getArguments().getInt("id", -1);
        if(id>=0)
        {
            TypeTable typeTable = DBHelper.getTypeById(id);

            title.setText(typeTable.getName());
            picker.setColor(typeTable.getColor());
            picker.setOldCenterColor(typeTable.getColor());
            picker.setShowOldCenterColor(true);

            parentId = typeTable.getParentId();
            if(DBHelper.getTypeTableByParentId(id).size()!=0) {
                listTypes.clear();
            }
            else {
                for (int i = 0; i < listTypes.size(); i++) {
                    TypeTable type = listTypes.get(i);
                    if (type.getId() == id) {
                        listTypes.remove(i);
                        continue;
                    }
                    if (type.getId() == parentId) {
                        parentId = i;
                        break;
                    }
                }
            }
        }

        listTypes.add(0, new TypeTable(-1, getString(R.string.type_title), 0, 0));
        parentSpinner = (Spinner) form.findViewById(R.id.type_spinner);
        parentSpinner.setAdapter(new MyTypeSpinnerAdapter(getContext(), listTypes));
        parentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    picker.setVisibility(View.VISIBLE);
                }
                else {
                    picker.setVisibility(View.GONE);
                    picker.setColor(listTypes.get(position-1).getColor());
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        parentSpinner.setSelection(parentId+1);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return(builder.setView(form)
                //.setTitle(R.string.type_title)
                .setPositiveButton(R.string.item_save, this)
                .setNegativeButton(R.string.cancel, null).create());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (TypeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement TypeDialogListener");
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        String name = title.getText().toString();
        TypeTable parent = (TypeTable)parentSpinner.getSelectedItem();

        if(!name.isEmpty()) {
            if(id<0) {
                DBHelper.addType(name, picker.getColor(), parent.getId());
                Toast.makeText(getActivity(), "Категория <" + name + "> добавлена", Toast.LENGTH_SHORT).show();
            }
            else {
                DBHelper.updateType(id, name, picker.getColor(), parent.getId());
            }

            mListener.onDialogPositiveClick(TypeDialogFragment.this);
        }
        else {
            Toast.makeText(getContext(), "Пустое название, категория не добавлена.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDismiss(DialogInterface unused) {
        super.onDismiss(unused);
    }
    @Override
    public void onCancel(DialogInterface unused) {
        super.onCancel(unused);
    }

    private class MyTypeSpinnerAdapter extends ArrayAdapter<TypeTable> {

        public MyTypeSpinnerAdapter(Context context, List<TypeTable> types) {
            super(context, android.R.layout.simple_list_item_2, types);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        // This funtion called for each row ( Called data.size() times )
        public View getCustomView(int position, View convertView, ViewGroup parent) {

            TypeTable type = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.type_spinner_item, null);
            }

            ((TextView) convertView.findViewById(R.id.type_list_title)).setText(type.getName());
            ((ImageView) convertView.findViewById(R.id.type_list_icon)).setImageDrawable(TextDrawable.builder().buildRound("", type.getColor()));

            return convertView;

        }
    }
}
