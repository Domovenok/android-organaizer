package ru.industries.tomat.organiser.TaskList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IExpandable;
import eu.davidea.flexibleadapter.items.ISectionable;
import eu.davidea.flexibleadapter.utils.Utils;
import eu.davidea.viewholders.ExpandableViewHolder;
import ru.industries.tomat.organiser.R;

/**
 * This is an experiment to evaluate how a Section with header can also be expanded/collapsed.
 * <p>Here, it still benefits of the common fields declared in AbstractModelItem.</p>
 * It's important to note that, the ViewHolder must be specified in all &lt;diamond&gt; signature.
 */
public class TaskListExpandableParent
        extends AbstractTaskItem<TaskListExpandableParent.ParentViewHolder>
        implements IExpandable<TaskListExpandableParent.ParentViewHolder, SubItem>, ISectionable<TaskListExpandableParent.ParentViewHolder, HeaderItem> {

    private static final long serialVersionUID = -1882711111814491060L;

    /* Flags for FlexibleAdapter */
    private boolean mExpanded = false;

    /* subItems list */
    private List<SubItem> mSubItems;

    //data
    private int color;
    HeaderItem header;

    public TaskListExpandableParent(String id) {
        super(id);
    }

    public int getColor(){
        return color;
    }

    public void setColor(int color){
        this.color = color;
    }

    @Override
    public boolean isSwipeable(){
        return true;
    }

    @Override
    public HeaderItem getHeader() {
        return header;
    }

    @Override
    public void setHeader(HeaderItem header) {
        this.header = header;
    }

    @Override
    public boolean isExpanded() {
        return mExpanded;
    }

    @Override
    public void setExpanded(boolean expanded) {
        mExpanded = expanded;
    }

    @Override
    public int getExpansionLevel() {
        return 1;
    }//This allows +1 level of expansion

    @Override
    public List<SubItem> getSubItems() {
        return mSubItems;
    }

    public final boolean hasSubItems() {
        return mSubItems!= null && mSubItems.size() > 0;
    }

    public boolean removeSubItem(SubItem item) {
        return item != null && mSubItems.remove(item);
    }

    public boolean removeSubItem(int position) {
        if (mSubItems != null && position >= 0 && position < mSubItems.size()) {
            mSubItems.remove(position);
            return true;
        }
        return false;
    }

    public void addSubItem(SubItem subItem) {
        if (mSubItems == null)
            mSubItems = new ArrayList<SubItem>();
        mSubItems.add(subItem);
    }

    public void addSubItem(int position, SubItem subItem) {
        if (mSubItems != null && position >= 0 && position < mSubItems.size()) {
            mSubItems.add(position, subItem);
        } else
            addSubItem(subItem);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_task_item;
    }

    @Override
    public ParentViewHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {
        return new ParentViewHolder(inflater.inflate(getLayoutRes(), parent, false), adapter);
    }

    @Override
    public void bindViewHolder(final FlexibleAdapter adapter, ParentViewHolder holder, int position, List payloads) {
        if (payloads.size() > 0) {
            Log.i(this.getClass().getSimpleName(), "ExpandableHeaderItem Payload " + payloads);
        } else {
            holder.mTitleView.setText(getTitle());
        }

        holder.mDateView.setText(getDateTime());

        String badge = " ";
        if(mSubItems!=null){
            badge = String.valueOf(mSubItems.size());
        }
        holder.mIcon.setImageDrawable(TextDrawable.builder().buildRound(badge, getColor()));

        //This is just an example of what you can do with item animation
        adapter.animateView(holder.itemView, position, adapter.isSelected(position));
    }

    @Override
    public String toString() {
        return "ExpandableLevel-1[" + super.toString() + "//SubItems" + mSubItems + "]";
    }

    static final class ParentViewHolder extends ExpandableViewHolder {

        public View mView;
        public TextView mTitleView;
        public TextView mDateView;
        public ImageView mIcon;
        public Context mContext;
        private View frontView;
        private View rearLeftView;
        private View rearRightView;


        public ParentViewHolder(View view, FlexibleAdapter adapter) {
            super(view, adapter);
            this.mContext = view.getContext();
            mView = view;
            this.mTitleView = (TextView) view.findViewById(R.id.task_item_title);
            this.mDateView = (TextView) view.findViewById(R.id.task_item_date);
            this.mIcon = (ImageView) view.findViewById(R.id.task_item_icon);

            this.frontView = view.findViewById(R.id.front_view);
            this.rearLeftView = view.findViewById(R.id.rear_left_view);
            this.rearRightView = view.findViewById(R.id.rear_right_view);
        }

        @Override
        public void onClick(View view) {
            //Toast.makeText(mContext, "Click on " + mTitleView.getText() + " position " + getAdapterPosition(), Toast.LENGTH_SHORT).show();
            super.onClick(view);
        }

        @Override
        public boolean onLongClick(View view) {
            //Toast.makeText(mContext, "LongClick on " + mTitleView.getText() + " position " + getAdapterPosition(), Toast.LENGTH_SHORT).show();
            return super.onLongClick(view);
        }

        @Override
        protected void toggleActivation() {
            super.toggleActivation();
        }

        @Override
        public float getActivationElevation() {
            return  0;//Utils.dpToPx(itemView.getContext(), 0f);
        }

        @Override
        protected boolean shouldActivateViewWhileSwiping() {
            return false;//default=false
        }

        @Override
        protected boolean shouldAddSelectionInActionMode() {
            return false;//default=false
        }

        @Override
        public View getFrontView() {
            return frontView;
        }

        @Override
        public View getRearLeftView() {
            return rearLeftView;
        }

        @Override
        public View getRearRightView() {
            return rearRightView;
        }

    }

}