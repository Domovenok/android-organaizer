package ru.industries.tomat.organiser;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by domov on 28.04.2016.
 */
@Table(database = MyAppDataBase.class)
public class TaskTable extends BaseModel {

    public static final int TASK_STATE_NORMAL = 0;
    public static final int TASK_STATE_COMPLETE = 1;
    public static final int TASK_STATE_BIN = 2;


    @Column
    @PrimaryKey(autoincrement = true)
    int id;

    @Column
    String name;

    @Column
    int typeId;

    @Column
    long date;

    @Column
    boolean notify;

    @Column
    int time;

    @Column
    String description;

    @Column
    int parentId;

    @Column
    int state;

    public int getId() { return id; }

    public String getName()
    {
        return name;
    }

    public int getTypeId() { return typeId; }

    public long getDateMili() { return date; }

    public String toString() { return name;}

    public int getParentId() {return parentId;}

    public String getDescription() {return description;}

    public int getState() {return state; }

    public void setState(int state) { this.state = state; }
}
