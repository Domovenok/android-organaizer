package ru.industries.tomat.organiser;

import android.support.v7.widget.RecyclerView;

import java.io.Serializable;

import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;

/**
 * This class will benefit of the already implemented methods (getter and setters) in
 * {@link eu.davidea.flexibleadapter.items.AbstractFlexibleItem}.
 *
 * It is used as Base item for all example models.
 */
public abstract class AbstractTypeItem<VH extends RecyclerView.ViewHolder>
        extends AbstractFlexibleItem<VH>
        implements Serializable {

    private String id;
    private String title;
    private int color;

    public AbstractTypeItem(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object inObject) {
        if (inObject instanceof AbstractTypeItem) {
            AbstractTypeItem inItem = (AbstractTypeItem) inObject;
            return this.id.equals(inItem.id);
        }
        return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", title=" + title;
    }

}