package ru.industries.tomat.organiser;

import com.raizlabs.android.dbflow.sql.language.From;
import com.raizlabs.android.dbflow.sql.language.SQLCondition;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

/**
 * Created by domov on 04.05.2016.
 */
public class DBHelper {

    DBHelper(){}

    static public List<TaskTable> getTaskTable()
    {
        return SQLite.select()
                .from(TaskTable.class)
                .queryResults().toList();
    }

    static public List<TaskTable> getTaskTableSortedByDate()
    {
        return SQLite.select()
                .from(TaskTable.class)
                .orderBy(TaskTable_Table.date, true)
                .queryList();
    }

    static public List<TaskTable> getTaskTableFilterByParentId(int parent)
    {
        return SQLite.select()
                .from(TaskTable.class)
                .where(TaskTable_Table.parentId.is(parent))
                .and(TaskTable_Table.state.is(TaskTable.TASK_STATE_NORMAL))
                .orderBy(TaskTable_Table.date, true)
                .queryList();
    }

    static public List<TaskTable> getTaskTableFilterByParentIdDate(int parent, long begin, long end)
    {
        return SQLite.select()
                .from(TaskTable.class)
                .where(TaskTable_Table.parentId.is(parent))
                .and(TaskTable_Table.date.between(begin).and(end))
                .and(TaskTable_Table.state.is(TaskTable.TASK_STATE_NORMAL))
                .orderBy(TaskTable_Table.date, true)
                .queryList();
    }

    static public List<TaskTable> getTaskTableFilterByParentIdAndType(int type, int parent)
    {
        return SQLite.select()
                .from(TaskTable.class)
                .where(TaskTable_Table.parentId.is(parent))
                .and(TaskTable_Table.typeId.is(type))
                .and(TaskTable_Table.state.is(TaskTable.TASK_STATE_NORMAL))
                .orderBy(TaskTable_Table.date, true)
                .queryList();
    }

    static public List<TaskTable> getTaskTableFilterByParentIdAndTypeDate(int type, int parent, long begin, long end)
    {
        if(type==-1){
            return getTaskTableFilterByParentIdDate(parent, begin, end);
        }
        return SQLite.select()
                .from(TaskTable.class)
                .where(TaskTable_Table.parentId.is(parent))
                .and(TaskTable_Table.typeId.is(type))
                .and(TaskTable_Table.date.between(begin).and(end))
                .and(TaskTable_Table.state.is(TaskTable.TASK_STATE_NORMAL))
                .orderBy(TaskTable_Table.date, true)
                .queryList();
    }

    static public List<TaskTable> getTaskTableFilterByTypeId(int id)
    {
        return SQLite.select()
                .from(TaskTable.class)
                .where(TaskTable_Table.typeId.is(id))
                .orderBy(TaskTable_Table.date, true)
                .queryList();
    }

    static public List<TaskTable> getTaskTableFilterByState(int state){
        return SQLite.select()
                .from(TaskTable.class)
                .where(TaskTable_Table.state.is(state))
                .orderBy(TaskTable_Table.date, true)
                .queryList();
    }

    static public TaskTable getTaskById(int id)
    {
        return SQLite.select()
                .from(TaskTable.class)
                .where(TaskTable_Table.id.is(id))
                .querySingle();
    }

    static public void addTask(String name, long date, int id, boolean notify, int time, String description, int parentId)
    {
        TaskTable TT = new TaskTable();
        TT.name = name;
        TT.date = date;
        TT.typeId = id;
        TT.notify = notify;
        TT.time = time;
        TT.description = description;
        TT.parentId = parentId;
        TT.state = TaskTable.TASK_STATE_NORMAL;
        TT.save();
    }

    static public void updateTask(int id, String name, long date, int typeId, boolean notify, int time, String description, int parentId)
    {
        TaskTable TT = SQLite.select().from(TaskTable.class).where(TaskTable_Table.id.is(id)).querySingle();
        TT.name = name;
        TT.date = date;
        TT.typeId = typeId;
        TT.notify = notify;
        TT.time = time;
        TT.description = description;
        TT.parentId = parentId;
        TT.save();
    }



    static public void delLastTask(int n)
    {
        List<TaskTable> lTask = getTaskTable();

        int del = Math.min(n, lTask.size());

        for(int i=1; i<=del; i++)
        {
            lTask.get(lTask.size()-i).delete();
        }
    }

    static public List<TypeTable> getTypeTable()
    {
        return SQLite.select()
                .from(TypeTable.class)
                .queryResults().toList();
    }
    static public List<TypeTable> getTypeTableByParentId(int parentId)
    {
        return SQLite.select()
                .from(TypeTable.class)
                .where(TypeTable_Table.parentId.is(parentId))
                .queryList();
    }

    static public TypeTable getTypeById(int id)
    {
        return SQLite.select()
                .from(TypeTable.class)
                .where(TypeTable_Table.id.is(id))
                .querySingle();
    }

    static public void addType(String name, int color, int parentId)
    {
        TypeTable TT = new TypeTable();
        TT.name = name;
        TT.color = color;
        TT.parentId = parentId;
        TT.save();
    }

    static public void delType(int id)
    {
        TypeTable TT = SQLite.select()
                .from(TypeTable.class)
                .where(TypeTable_Table.id.is(id))
                .querySingle();
        TT.delete();
    }

    static public void updateType(int id, String name, int color, int parentId)
    {
        TypeTable TT = SQLite.select()
                .from(TypeTable.class)
                .where(TypeTable_Table.id.is(id))
                .querySingle();
        TT.name = name;
        TT.color = color;
        TT.parentId = parentId;
        TT.update();
    }

    static public void genRandomTasks(int n)
    {
        String[] RandNameGenerator = {"Один","Два","Три","Четыре","Пять","Шесть","Семь","Восемь","Девять","Десять","Одиннацать","Двенадцать","Тринадцать"};

        Random Rnd = new Random();
        List<TypeTable> lType = getTypeTable();
        Calendar cal = Calendar.getInstance();

        for(int i=0; i<n; i++)
        {
            int stcount = Rnd.nextInt(RandNameGenerator.length)+1;
            StringBuilder title = new StringBuilder();
            title.append(RandNameGenerator[Rnd.nextInt(RandNameGenerator.length)]);
            for(int j=1; j<stcount; j++)
            {
                title.append(" ");
                title.append(RandNameGenerator[Rnd.nextInt(RandNameGenerator.length)].toLowerCase());
            }

            cal.add(Calendar.DAY_OF_YEAR, Rnd.nextBoolean()?-Rnd.nextInt(100):Rnd.nextInt(100));

            int rId = lType.get(Rnd.nextInt(lType.size())).getId();

            addTask(title.toString(), cal.getTimeInMillis(), rId, false, 0, "", -1);
        }

    }

}
