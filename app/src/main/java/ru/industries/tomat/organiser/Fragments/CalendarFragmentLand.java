package ru.industries.tomat.organiser.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.amulyakhare.textdrawable.TextDrawable;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.SmoothScrollLinearLayoutManager;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import ru.industries.tomat.organiser.Activities.MainActivity;
import ru.industries.tomat.organiser.Activities.TaskActivity;
import ru.industries.tomat.organiser.Animation.SlideInRightAnimator;
import ru.industries.tomat.organiser.Application.MyApplication;
import ru.industries.tomat.organiser.CalendarItem;
import ru.industries.tomat.organiser.DBHelper;
import ru.industries.tomat.organiser.R;
import ru.industries.tomat.organiser.TaskList.AbstractTaskItem;
import ru.industries.tomat.organiser.TaskList.HeaderItem;
import ru.industries.tomat.organiser.TaskList.SubItem;
import ru.industries.tomat.organiser.TaskList.TaskListExpandableParent;
import ru.industries.tomat.organiser.TaskTable;
import ru.industries.tomat.organiser.TypeTable;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CalendarFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CalendarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalendarFragmentLand extends Fragment implements MainActivity.MainActivityListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CaldroidFragment calFrag = null;
    private TaskFragment taskFrag = null;
    private OnFragmentInteractionListener mListener;
    private FragmentTransaction fTrans = null;

    private FrameLayout frameCal;
    private FrameLayout frameTask;

    private int lastIndx = -1;
    private Calendar curMonth = Calendar.getInstance();
    private int type = -1;

    private List<Date> listDate = new ArrayList<>();
    private ArrayList<Integer> listCount = new ArrayList<>();

    private List<TaskTable> tasks = null;

    private FlexibleAdapter myAdapter = null;
    private RecyclerView myRecycler = null;
    public static List<AbstractFlexibleItem> mItems = new ArrayList<AbstractFlexibleItem>();

    private CalendarItem calItem = null;

    public CalendarFragmentLand() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CalendarFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CalendarFragmentLand newInstance(String param1, String param2) {
        CalendarFragmentLand fragment = new CalendarFragmentLand();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        initCalFrag();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_calendar, calFrag);
        transaction.commitAllowingStateLoss();

        //onTaskAdded();

        Log.i("метка", getContext().toString());

        return view;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void initCalFrag(){
        calFrag = new CaldroidFragment();
        Bundle args = new Bundle();
        args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, false);
        args.putInt(CaldroidFragment.START_DAY_OF_WEEK, CaldroidFragment.MONDAY);
        //args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, false);
        calFrag.setArguments(args);

        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {

                //Clear selection and select new date
                selectDate(date);
            }

            @Override
            public void onChangeMonth(int month, int year) {

                //TODO ставить баджики на текущий месяц
                curMonth.clear();
                curMonth.set(year, month, 0);
                onTaskAdded();

            }

            @Override
            public void onLongClickDate(Date date, View view) {

            }
        };

        // Setup Caldroid
        calFrag.setCaldroidListener(listener);
    }

    public void onTaskAdded()
    {
        Calendar badg = Calendar.getInstance();
        badg.set(curMonth.get(Calendar.YEAR), curMonth.get(Calendar.MONTH), 0);
        long begin, end;

        badg.add(Calendar.MONTH, -1);
        begin = badg.getTimeInMillis();
        badg.add(Calendar.MONTH, 2);
        end = badg.getTimeInMillis() - 1;

        List<TaskTable> tasks = DBHelper.getTaskTableFilterByParentIdAndTypeDate(type, -1, begin, end);

        int i = 0;
        listCount.clear();
        listDate.clear();
        lastIndx = -1;

        for(TaskTable task:tasks){

            badg.setTimeInMillis(task.getDateMili());
            badg.set(Calendar.HOUR_OF_DAY, 1);
            badg.clear(Calendar.MINUTE);
            badg.clear(Calendar.SECOND);
            badg.clear(Calendar.MILLISECOND);

            if (i == 0) {
                listDate.add(badg.getTime());
                listCount.add(1);
                i++;
            } else {

                if (listDate.get(i - 1).equals(badg.getTime())) {

                    listCount.set(i - 1, listCount.get(i - 1) + 1);

                } else {
                    listDate.add(badg.getTime());
                    listCount.add(1);
                    i++;
                }
            }
        }
        setBudgets();
    }

    public void onTypeSelected(int id)
    {
        type = id;
        onTaskAdded();
        //Фильтр по айди типа
    }

    private Drawable drawBadget(int count, int color){

        //TODO Разберись с этой матрицей подгона
        Drawable drawableArray[]= new Drawable[]{new ColorDrawable(color), TextDrawable.builder().buildRoundRect(String.valueOf(count), Color.RED, 15)};
        LayerDrawable layerDraw = new LayerDrawable(drawableArray);

        Resources resources = getContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();

        int pxpdp = Math.round((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        int vert = 32 * pxpdp;
        int horiz = 12 * pxpdp;

        layerDraw.setLayerInset(1, vert, 0, 0, horiz);

        return layerDraw;
    }

    private void setBudgets(){

        HashMap<Date, Drawable> mapa = new HashMap<Date, Drawable>();

        for(int i=0; i<listDate.size(); i++){
            mapa.put(listDate.get(i), drawBadget(listCount.get(i), ContextCompat.getColor(getContext(), R.color.md_light_blue_100)));
        }

        calFrag.setBackgroundDrawableForDates(mapa);
        calFrag.refreshView();

    }

    private void setBudget(int indx, int color){
        calFrag.setBackgroundDrawableForDate(drawBadget(listCount.get(indx), color), listDate.get(indx));
        calFrag.refreshView();
    }

    private void selectDate(Date date){

        Calendar badg = Calendar.getInstance();
        badg.setTimeInMillis(date.getTime());
        badg.set(Calendar.HOUR_OF_DAY, 1);
        badg.clear(Calendar.MINUTE);
        badg.clear(Calendar.SECOND);
        badg.clear(Calendar.MILLISECOND);

        int indx = listDate.indexOf(badg.getTime());
        if(indx>=0 && indx!=lastIndx){
            //initTaskList(date.getTime());
            setBudget(indx, ContextCompat.getColor(getContext(), R.color.md_green_600));
            if(lastIndx>=0) {
                setBudget(lastIndx, ContextCompat.getColor(getContext(), R.color.md_light_blue_100));
            }
            lastIndx = indx;
        }
    }

}

