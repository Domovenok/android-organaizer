package ru.industries.tomat.organiser.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.DividerItemDecoration;
import eu.davidea.flexibleadapter.common.SmoothScrollLinearLayoutManager;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import ru.industries.tomat.organiser.DBHelper;
import ru.industries.tomat.organiser.R;
import ru.industries.tomat.organiser.Animation.SlideInRightAnimator;
import ru.industries.tomat.organiser.TaskList.SubItem;
import ru.industries.tomat.organiser.TaskList.TaskListExpandableParent;
import ru.industries.tomat.organiser.TaskTable;
import ru.industries.tomat.organiser.TypeTable;

public class RoCActivity extends AppCompatActivity {

    private FlexibleAdapter myAdapter;
    private RecyclerView myRecycler;

    public static final List<AbstractFlexibleItem> mItems = new ArrayList<AbstractFlexibleItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ro_c);

        Bundle extras = getIntent().getExtras();
        int taskState = 0;
        if(extras!=null) {
            taskState = extras.getInt("Task-State");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(taskState==1?R.string.archive:R.string.bin);

        setContext(taskState);
        initializeRecyclerView();
    }

    public void initializeRecyclerView(){
        myAdapter = new FlexibleAdapter(mItems, this);

        myAdapter.setAnimationOnScrolling(true);
        myAdapter.setAnimationOnReverseScrolling(true);
        myAdapter.setAutoCollapseOnExpand(false);
        myAdapter.setMinCollapsibleLevel(1);//Auto-collapse only items with level >= 1 (avoid to collapse also sections!)
        myAdapter.setAutoScrollOnExpand(true);
        myAdapter.setRemoveOrphanHeaders(false);

        myRecycler = (RecyclerView) findViewById(R.id.recycler_view);
        myRecycler.setLayoutManager(new SmoothScrollLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        myRecycler.setAdapter(myAdapter);
        myRecycler.setHasFixedSize(true); //Size of RV will not change
        myRecycler.setItemAnimator(new DefaultItemAnimator() {
            @Override
            public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
                //NOTE: This allows to receive Payload objects on notifyItemChanged called by the Adapter!!!
                return true;
            }
        });
        myRecycler.setItemAnimator(new SlideInRightAnimator());
        myRecycler.addItemDecoration(new DividerItemDecoration(this, R.drawable.divider));

        //myAdapter.setSwipeEnabled(true);//Enable swipe items
    }

    public void setContext(int state){

        mItems.clear();

        Calendar cal = Calendar.getInstance();

        List<TaskTable> mainTasks = DBHelper.getTaskTableFilterByState(state);

        for (TaskTable TT: mainTasks) {

            TypeTable type = DBHelper.getTypeById(TT.getTypeId());
            cal.setTimeInMillis(TT.getDateMili());

            TaskListExpandableParent parentTask = new TaskListExpandableParent(String.valueOf(TT.getId()));
            parentTask.setTitle(TT.getName());
            parentTask.setDateTime(MainActivity.dateTimeForm.format(cal.getTime()));
            parentTask.setColor(type.getColor());

            List<TaskTable> childs = DBHelper.getTaskTableFilterByParentId(TT.getId());

            for(TaskTable child: childs) {

                SubItem subTask = new SubItem(String.valueOf(child.getId()));
                subTask.setTitle(child.getName());

                cal.setTimeInMillis(child.getDateMili());
                subTask.setDateTime(MainActivity.dateTimeForm.format(cal.getTime()));

                parentTask.addSubItem(subTask);
            }

            mItems.add(parentTask);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
