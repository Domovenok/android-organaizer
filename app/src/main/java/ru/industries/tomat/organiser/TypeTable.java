package ru.industries.tomat.organiser;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by domov on 28.04.2016.
 */
@Table(database = MyAppDataBase.class)
public class TypeTable extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = true)
    int id;

    @Column
    String name;

    @Column
    int color;

    @Column
    int parentId;

    public TypeTable(){}

    public TypeTable(int id, String name, int color ,int parentId){
        this.id = id;
        this.name = name;
        this.color = color;
        this.parentId = parentId;
    }

    public void setRow(String name) {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }

    public int getColor() { return this.color; }

    public int getId()
    {
        return this.id;
    }

    public int getParentId() {return this.parentId;}
}
