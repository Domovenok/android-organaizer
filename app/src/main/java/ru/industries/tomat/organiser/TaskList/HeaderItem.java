package ru.industries.tomat.organiser.TaskList;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.AbstractHeaderItem;
import eu.davidea.flexibleadapter.items.IFilterable;
import eu.davidea.viewholders.FlexibleViewHolder;
import ru.industries.tomat.organiser.R;

/**
 * This is a simple item with custom layout for headers.
 * <p>A Section should not contain others Sections!</p>
 * Headers are not Sectionable!
 */
public class HeaderItem extends AbstractHeaderItem<HeaderItem.HeaderViewHolder> implements IFilterable {

    private static final long serialVersionUID = -7408637077727563374L;

    private String id;

    public HeaderItem(String id) {
        super();
        this.id = id;
    }

    @Override
    public boolean equals(Object inObject) {
        if (inObject instanceof HeaderItem) {
            HeaderItem inItem = (HeaderItem) inObject;
            return this.getId().equals(inItem.getId());
        }
        return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_task_date;
    }

    @Override
    public HeaderViewHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {
        return new HeaderViewHolder(inflater.inflate(getLayoutRes(), parent, false), adapter);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void bindViewHolder(FlexibleAdapter adapter, HeaderViewHolder holder, int position, List payloads) {
        if (payloads.size() > 0) {
            Log.i(this.getClass().getSimpleName(), "HeaderItem Payload " + payloads);
        } else {
            holder.mTitleView.setText(getId());
        }
    }

    @Override
    public boolean filter(String constraint) {
        return getId() != null && getId().toLowerCase().trim().contains(constraint);
    }

    static class HeaderViewHolder extends FlexibleViewHolder {

        public final View mView;
        public final TextView mTitleView;

        public HeaderViewHolder(View view, FlexibleAdapter adapter) {
            super(view, adapter);

            mView = view;
            mTitleView = (TextView) view.findViewById(R.id.task_list_date);

            mTitleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("HeaderTitle", "Registered internal click on Header TitleTextView! " + mTitleView.getText() + " position=" + getFlexibleAdapterPosition());
                }
            });
        }
    }

    @Override
    public String toString() {
        return "HeaderItem[id=" + id;
    }

}
