package ru.industries.tomat.organiser.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.github.clans.fab.FloatingActionButton;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.roomorama.caldroid.CaldroidFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ru.industries.tomat.organiser.DBHelper;
import ru.industries.tomat.organiser.Fragments.CalendarFragment;
import ru.industries.tomat.organiser.Fragments.CalendarFragmentLand;
import ru.industries.tomat.organiser.Fragments.TypeDialogFragment;
import ru.industries.tomat.organiser.Fragments.TaskFragment;
import ru.industries.tomat.organiser.MyViewPager;
import ru.industries.tomat.organiser.R;
import ru.industries.tomat.organiser.TaskTable;
import ru.industries.tomat.organiser.TypeTable;
import ru.industries.tomat.organiser.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity implements TypeDialogFragment.TypeDialogListener {

    private Drawer myDrawer = null;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private MyViewPager viewPager;

    private MainActivityListener mTaskListener;
    private MainActivityListener mCalListener;

    private CalendarFragment cf = null;
    private CalendarFragmentLand cfl = null;
    private TaskFragment tf = null;

    //Отображение даты и времени
    public static final SimpleDateFormat dateTimeForm = new SimpleDateFormat("HH:mm dd.MM.yyyy");
    public static final SimpleDateFormat timeForm = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat dateForm = new SimpleDateFormat("dd.MM.yyyy");

    private TextDrawable.IBuilder mDrawableBuilder = TextDrawable.builder().round();

    public interface MainActivityListener{
        void onTaskAdded();
        void onTypeSelected(int id);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        init_Drawer(savedInstanceState);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewTaskActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        switch(screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                setupPortrait();
                break;
            default:
                setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                setupLandscape();
        }

    }

    private void init_Drawer(Bundle savedInstanceState) {

        myDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withSavedInstance(savedInstanceState)
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true)
                .withDrawerLayout(R.layout.material_drawer_fits_not)
                .addStickyDrawerItems(
                        new PrimaryDrawerItem().withName("Все задачи").withIdentifier(-1), //TODO Иконочку
                        new PrimaryDrawerItem().withName("Новая категория").withIdentifier(-2).withIcon(R.drawable.ic_content_add)//,
                        //new SecondaryDrawerItem().withName("Создать 10 случайных задач").withIdentifier(-3),
                        //new SecondaryDrawerItem().withName("Удалить 10 последних задач").withIdentifier(-4)
                )
                .withOnDrawerItemLongClickListener(new Drawer.OnDrawerItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            if (drawerItem.getIdentifier() >= 0) {
                                showTypeDialog((int)drawerItem.getIdentifier());
                                Toast.makeText(MainActivity.this, String.valueOf(drawerItem.getIdentifier()), Toast.LENGTH_SHORT).show();
                            }
                        }
                        return false;
                    }
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem instanceof Nameable) {
                            int id = (int)drawerItem.getIdentifier();
                            switch (id){
                                case -1:
                                    mTaskListener.onTypeSelected(-1);
                                    mCalListener.onTypeSelected(-1);
                                    return false;
                                case -2:
                                    showTypeDialog(-1);
                                    return false;
                                case -3:
                                    DBHelper.genRandomTasks(10);
                                    mTaskListener.onTaskAdded();
                                    mCalListener.onTaskAdded();
                                    return false;
                                case -4:
                                    DBHelper.delLastTask(10);
                                    mTaskListener.onTaskAdded();
                                    mCalListener.onTaskAdded();
                                    return false;
                                case -5:
                                    //Архив
                                    Intent archive = new Intent(getActivity(), RoCActivity.class);
                                    archive.putExtra("Task-State", TaskTable.TASK_STATE_COMPLETE);
                                    startActivity(archive);
                                    return false;
                                case -6:
                                    //Корзина
                                    Intent bin = new Intent(getActivity(), RoCActivity.class);
                                    bin.putExtra("Task-State", TaskTable.TASK_STATE_BIN);
                                    startActivity(bin);
                                    return false;
                                default:
                                    mTaskListener.onTypeSelected((int)drawerItem.getIdentifier());
                                    mCalListener.onTypeSelected((int)drawerItem.getIdentifier());
                            }
                        }
                        return false;
                    }
                }).build();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        updateDrawer();
    }

    private void updateDrawer() {

        myDrawer.removeAllItems();
        List<TypeTable> LTT = DBHelper.getTypeTableByParentId(-1);

        for (TypeTable TT : LTT) {

            List<IDrawerItem> childs = new ArrayList<IDrawerItem>();

            List<TypeTable> LChilds = DBHelper.getTypeTableByParentId(TT.getId());

            if(LChilds.size()>0) {

            for (TypeTable child : LChilds) {
                childs.add(new PrimaryDrawerItem()
                        .withName(child.getName())
                        .withIcon(mDrawableBuilder.build(String.valueOf(child.getName().charAt(0)), TT.getColor()))
                        .withLevel(2)
                        .withIdentifier(child.getId()));
            }

            //childs.add(new SectionDrawerItem());

            myDrawer.addItem(new ExpandableDrawerItem()
                    .withName(TT.getName())
                    .withIcon(mDrawableBuilder.build(" ", TT.getColor()))
                    .withIdentifier(TT.getId())
                    .withSubItems(childs));
            } else {
                myDrawer.addItem(new PrimaryDrawerItem()
                        .withName(TT.getName())
                        .withIcon(mDrawableBuilder.build(" ", TT.getColor()))
                        .withIdentifier(TT.getId()));
            }
        }

        myDrawer.addItem(new SectionDrawerItem().withName(""));
        myDrawer.addItem(new PrimaryDrawerItem()
                .withName(R.string.archive)
                .withIcon(R.drawable.ic_content_archive)
                .withIdentifier(-5));
        myDrawer.addItem(new PrimaryDrawerItem()
                .withName(R.string.bin)
                .withIcon(R.drawable.ic_action_delete)
                .withIdentifier(-6));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == RESULT_OK) {
            mTaskListener.onTaskAdded();
            mCalListener.onTaskAdded();
        }
    }

    public void showTypeDialog(int id) {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new TypeDialogFragment();

        Bundle args = new Bundle();
        args.putInt("id", id);
        dialog.setArguments(args);

        dialog.show(getSupportFragmentManager(), "type");
    }

    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        updateDrawer();
    }

    private void setupLandscape(){

        cfl = new CalendarFragmentLand();
        mCalListener = cfl;

        tf = new TaskFragment();
        mTaskListener = tf;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.layout_cal, cfl);
        transaction.replace(R.id.layout_list, tf);
        transaction.commit();

    }

    private void setupPortrait(){


        viewPager = (MyViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void setupViewPager(MyViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        cf = new CalendarFragment();
        mCalListener = cf;

        tf = new TaskFragment();
        mTaskListener = tf;

        adapter.addFragment(cf, getString(R.string.tab1_name));
        adapter.addFragment(tf, getString(R.string.tab2_name));

        viewPager.setAdapter(adapter);
    }

    private Activity getActivity() {
        return this;
    }
}
