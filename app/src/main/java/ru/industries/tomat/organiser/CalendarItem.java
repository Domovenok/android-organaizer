package ru.industries.tomat.organiser;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.roomorama.caldroid.CaldroidFragment;

import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IHeader;
import eu.davidea.flexibleadapter.items.ISectionable;
import eu.davidea.flexibleadapter.utils.Utils;
import eu.davidea.viewholders.FlexibleViewHolder;
import ru.industries.tomat.organiser.TaskList.AbstractTaskItem;

/**
 * If you don't have many fields in common better to extend directly from
 * {@link eu.davidea.flexibleadapter.items.AbstractFlexibleItem} to benefit of the already
 * implemented methods (getter and setters).
 */
public class CalendarItem extends AbstractTaskItem<CalendarItem.ChildViewHolder> {

    private static final long serialVersionUID = 2519281529221254210L;

    Fragment fragParent;
    CaldroidFragment calendar;

    public CalendarItem(String id, Fragment parent, CaldroidFragment calendar) {
        super(id);
        this.fragParent = parent;
        this.calendar = calendar;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_calendar_item;
    }

    @Override
    public ChildViewHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {

        return new ChildViewHolder(inflater.inflate(getLayoutRes(), parent, false), adapter, fragParent, calendar);
    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, ChildViewHolder holder, int position, List payloads) {
        //In case of searchText matches with Title or with an SimpleItem's field
        // this will be highlighted
    }

    /**
     * Provide a reference to the views for each data item.
     * Complex data labels may need more than one view per item, and
     * you provide access to all the views for a data item in a view holder.
     */
    static final class ChildViewHolder extends FlexibleViewHolder {

        public ChildViewHolder(View view, FlexibleAdapter adapter, Fragment frag, CaldroidFragment calendar) {
            super(view, adapter);

            frag.getFragmentManager().beginTransaction().add(R.id.frame_calendar, calendar).commitAllowingStateLoss();

        }

        @Override
        public void onClick(View view) {
            super.onClick(view);
        }

        @Override
        public boolean onLongClick(View view) {
            return false;
        }

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            return false;
        }

        @Override
        public float getActivationElevation() {
            return 0;//Utils.dpToPx(itemView.getContext(), 4f);
        }

    }

    @Override
    public String toString() {
        return "SubItem[" + super.toString() + "]";
    }

}