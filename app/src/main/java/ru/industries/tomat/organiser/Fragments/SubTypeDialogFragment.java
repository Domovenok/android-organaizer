package ru.industries.tomat.organiser.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import java.util.List;

import ru.industries.tomat.organiser.DBHelper;
import ru.industries.tomat.organiser.R;
import ru.industries.tomat.organiser.TypeTable;


public class SubTypeDialogFragment extends DialogFragment implements
        DialogInterface.OnClickListener {


    public interface SubTypeDialogListener {
        public void onDialogPositiveClick(int SubTypeId);
    }

    // Use this instance of the interface to deliver action events
    SubTypeDialogListener mListener;
    List<TypeTable> subTypes = null;
    String[] names;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int parentid = getArguments().getInt("ParentID");

        if(parentid>=0){
            subTypes = DBHelper.getTypeTableByParentId(parentid);
        }

        names = new String[subTypes.size()+1];
        int i=0;
        names[i++] = "Без подкатегории";

        for(TypeTable subtype: subTypes){
            names[i++] = subtype.getName();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.subtype_select)
                .setItems(names, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if(which==0){
                            dialog.dismiss();
                            return;
                        }
                        mListener.onDialogPositiveClick(subTypes.get(which-1).getId());
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (SubTypeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement TypeDialogListener");
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
    }

    @Override
    public void onDismiss(DialogInterface unused) {
        super.onDismiss(unused);
    }

    @Override
    public void onCancel(DialogInterface unused) {
        super.onCancel(unused);
    }
}