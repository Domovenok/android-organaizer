package ru.industries.tomat.organiser.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.List;

import ru.industries.tomat.organiser.DBHelper;
import ru.industries.tomat.organiser.Fragments.CalendarFragment;
import ru.industries.tomat.organiser.Fragments.TaskFragment;
import ru.industries.tomat.organiser.MyViewPager;
import ru.industries.tomat.organiser.R;
import ru.industries.tomat.organiser.TaskTable;
import ru.industries.tomat.organiser.TypeTable;
import ru.industries.tomat.organiser.ViewPagerAdapter;

public class TaskActivity extends AppCompatActivity {

    private int taskId;

    private ImageView typeIcon;
    private TextView typeTitle;
    private TextView textName;
    private TextView textDateTime;
    private TextView textNotify;
    private TextView textDescription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
    }

    private boolean loadContent()
    {
        Bundle extras = getIntent().getExtras();
        taskId = extras.getInt("TaskID");

        if(taskId >= 0) {
            TaskTable task = DBHelper.getTaskById(taskId);
            TypeTable type = DBHelper.getTypeById(task.getTypeId());
            int color = type.getColor();
            String badge = "";
            if(type.getParentId()>=0){
                color = DBHelper.getTypeById(type.getParentId()).getColor();
                badge = String.valueOf(type.getName().charAt(0));
            }

            Calendar cur = Calendar.getInstance();
            cur.setTimeInMillis(task.getDateMili());

            ImageView typeIcon = (ImageView) findViewById(R.id.type_icon);
            TextView typeTitle = (TextView) findViewById(R.id.sel_type_text);
            TextView textName = (TextView) findViewById(R.id.editText_Name);
            TextView textDateTime = (TextView) findViewById(R.id.textDateTime);
            TextView textNotify = (TextView) findViewById(R.id.text_notify);
            TextView textDescription = (TextView) findViewById(R.id.editText_Desc);

            typeIcon.setImageDrawable(TextDrawable.builder().round().build(badge, color));
            typeTitle.setText(type.getName());

            textName.setText(task.getName());
            textDateTime.setText(MainActivity.dateTimeForm.format(cur.getTime()));
            //TODO textNotify
            textNotify.setText("Уведомление");
            textDescription.setText(task.getDescription());


            //childs
            List<TaskTable> childs = DBHelper.getTaskTableFilterByParentId(taskId);
            if(childs.size()>0){
                findViewById(R.id.task_subtask_text).setVisibility(View.VISIBLE);
                View linearLayout =  findViewById(R.id.activity_task_subtask);
                ((LinearLayout)linearLayout).removeAllViews();

                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openTaskActivity((int)v.getTag());
                    }
                };

                for(TaskTable child : childs){

                    Button valueTV = new Button(this);
                    valueTV.setText(child.getName());
                    valueTV.setTag(child.getId());
                    valueTV.setId(R.id.activity_task_subtask);
                    valueTV.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
                    valueTV.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);


                    valueTV.setPadding(10, 10, 10, 10);
                    valueTV.setOnClickListener(listener);

                    ((LinearLayout) linearLayout).addView(valueTV);
                }

            }

            return true;
        }
        return false;
    }

    @Override
    public void onResume(){
        super.onResume();
        loadContent();
    }

    private void openTaskActivity(int id){

        Intent myIntent = new Intent(this, TaskActivity.class);
        myIntent.putExtra("TaskID", id);
        startActivity(myIntent);
    }

    private void openNewTaskActivity(){

        Intent myIntent = new Intent(this, NewTaskActivity.class);
        myIntent.putExtra("TaskID", taskId);
        startActivity(myIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.button_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.edit_item:
                openNewTaskActivity();
                return true;
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
