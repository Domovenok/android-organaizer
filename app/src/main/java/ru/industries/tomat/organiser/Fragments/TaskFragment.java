package ru.industries.tomat.organiser.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.DividerItemDecoration;
import eu.davidea.flexibleadapter.common.SmoothScrollLinearLayoutManager;
import eu.davidea.flexibleadapter.helpers.UndoHelper;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.flexibleadapter.items.IFlexible;
import ru.industries.tomat.organiser.Activities.MainActivity;
import ru.industries.tomat.organiser.Activities.TaskActivity;
import ru.industries.tomat.organiser.Animation.SlideInRightAnimator;
import ru.industries.tomat.organiser.DBHelper;
import ru.industries.tomat.organiser.R;
import ru.industries.tomat.organiser.TaskList.AbstractTaskItem;
import ru.industries.tomat.organiser.TaskList.HeaderItem;
import ru.industries.tomat.organiser.TaskList.SubItem;
import ru.industries.tomat.organiser.TaskList.TaskListExpandableParent;
import ru.industries.tomat.organiser.TaskTable;
import ru.industries.tomat.organiser.TypeTable;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class TaskFragment extends Fragment implements MainActivity.MainActivityListener,
        FlexibleAdapter.OnItemLongClickListener,
        FlexibleAdapter.OnItemSwipeListener,
        UndoHelper.OnUndoListener {

    // TODO: Customize parameter argument names
    static final String ARG_PARENT_ID = "parent-id";
    static final String ARG_DATE_TIME = "date-time";
    // TODO: Customize parameters

    public static List<AbstractFlexibleItem> mItems = null;

    private FlexibleAdapter myAdapter = null;
    private RecyclerView myRecycler = null;
    private AbstractTaskItem swipedItem = null;
    private int lastSwipeAction = 0;

    private int parentId = -1;
    private int typeId = -1;
    private boolean day = false;

    private Calendar mCalendar = Calendar.getInstance();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TaskFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static TaskFragment newInstance(int parentId) {
        TaskFragment fragment = new TaskFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARENT_ID, parentId);
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("unused")
    public static TaskFragment newInstance(int parentId, long dateInMili) {
        TaskFragment fragment = new TaskFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARENT_ID, parentId);
        args.putLong(ARG_DATE_TIME, dateInMili);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null) {
            parentId = getArguments().getInt(ARG_PARENT_ID, -1);

            long date = getArguments().getLong(ARG_DATE_TIME, -1);
            if(date>=0){
                mCalendar.setTimeInMillis(date);
                day = true;
            }
        }

        mItems = new ArrayList<AbstractFlexibleItem>();
    }

    @Override
    public void onResume(){
        super.onResume();

        if(myAdapter!=null){
            myAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_list, container, false);

        myRecycler = (RecyclerView) view.findViewById(R.id.recycler_view);

        setContext();
        initializeRecyclerView();

        return view;
    }

    public void initializeRecyclerView(){
        myAdapter = new FlexibleAdapter(mItems, this);

        myAdapter.setAnimationOnScrolling(true);
        myAdapter.setAnimationOnReverseScrolling(true);
        myAdapter.setAutoCollapseOnExpand(false);
        myAdapter.setMinCollapsibleLevel(1);//Auto-collapse only items with level >= 1 (avoid to collapse also sections!)
        myAdapter.setAutoScrollOnExpand(true);
        myAdapter.setRemoveOrphanHeaders(false);

        myRecycler.setLayoutManager(new SmoothScrollLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        myRecycler.setAdapter(myAdapter);
        myRecycler.setHasFixedSize(true); //Size of RV will not change
        myRecycler.setItemAnimator(new DefaultItemAnimator() {
            @Override
            public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
                //NOTE: This allows to receive Payload objects on notifyItemChanged called by the Adapter!!!
                return true;
            }
        });
        myRecycler.setItemAnimator(new SlideInRightAnimator());
        //myRecycler.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));

        myAdapter.setSwipeEnabled(true);//Enable swipe items
        myAdapter.setDisplayHeadersAtStartUp(true);//Show Headers at startUp!
        //myAdapter.enableStickyHeaders();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void onTaskAdded()
    {
        setContext();
        if(myAdapter!=null) {
            myAdapter.notifyDataSetChanged();
            myAdapter.showAllHeaders();
        }

    }

    public void onTypeSelected(int id)
    {
        //TODO Фильтр по айди типа
        typeId = id;
        onTaskAdded();
    }

    public void onItemLongClick(int position)
    {

        AbstractTaskItem item = (AbstractTaskItem) myAdapter.getItem(position);

        Intent myIntent = new Intent(getContext(), TaskActivity.class);
        myIntent.putExtra("TaskID", (int)Integer.parseInt(item.getId()));
        startActivity(myIntent);

    }

    public void onItemSwipe(final int position, int direction){

        if(swipedItem!=null){
            onDeleteConfirmed(lastSwipeAction);
        }
        swipedItem = (AbstractTaskItem) myAdapter.getItem(position);

        List<Integer> positions = new ArrayList<Integer>(1);
        positions.add(position);
        StringBuilder message = new StringBuilder();

        if (direction == ItemTouchHelper.LEFT) {
            message.append(getString(R.string.action_complete));
            new UndoHelper(myAdapter, this)
                    .withPayload(true)
                    .withAction(UndoHelper.ACTION_UPDATE, new UndoHelper.OnActionListener() {
                        @Override
                        public boolean onPreAction() {
                            //Return true to avoid default early item deletion.
                            //Ask to the user what to do with a custom dialog. On option chosen,
                            //remove the item from Adapter list as usual.
                            lastSwipeAction = UndoHelper.ACTION_UPDATE;
                            myAdapter.removeItem(position);
                            return true;
                        }

                        @Override
                        public void onPostAction() {
                            //Nothing
                        }
                    })
                    .remove(positions,
                            getView().findViewById(R.id.task_list_frame), message,
                            getString(R.string.undo), UndoHelper.UNDO_TIMEOUT);

        } else if (direction == ItemTouchHelper.RIGHT) {
            message.append(getString(R.string.action_deleted));
            new UndoHelper(myAdapter, this)
                    .withPayload(true)
                    .withAction(UndoHelper.ACTION_REMOVE, new UndoHelper.OnActionListener() {
                        @Override
                        public boolean onPreAction() {
                            //Don't consume the event
                            lastSwipeAction = UndoHelper.ACTION_REMOVE;
                            myAdapter.removeItem(position);
                            return true;
                        }

                        @Override
                        public void onPostAction() {

                        }
                    })
                    .remove(positions,
                            getView().findViewById(R.id.task_list_frame), message,
                            getString(R.string.undo), UndoHelper.UNDO_TIMEOUT);

        }
    }

    public void onDeleteConfirmed(int action){

        TaskTable task = DBHelper.getTaskById(Integer.parseInt(swipedItem.getId()));

        switch (action){
            case UndoHelper.ACTION_UPDATE:

                task.setState(TaskTable.TASK_STATE_COMPLETE);

                break;
            case UndoHelper.ACTION_REMOVE:

                task.setState(TaskTable.TASK_STATE_BIN);

                break;
        }

        task.save();
        swipedItem = null;
    }

    public void onUndoConfirmed(int action){
        setContext();
        myAdapter.notifyDataSetChanged();
        swipedItem = null;

    }

    public void setContext(){

        mItems.clear();

        Calendar cal = Calendar.getInstance();

        List<TaskTable> mainTasks = getTaskTable();

        HeaderItem head = null;

        for (TaskTable TT: mainTasks) {

            TypeTable type = DBHelper.getTypeById(TT.getTypeId());
            int color = type.getColor();
            if(type.getParentId()>=0){
                color = DBHelper.getTypeById(type.getParentId()).getColor();
            }

            cal.setTimeInMillis(TT.getDateMili());

            TaskListExpandableParent parentTask = new TaskListExpandableParent(String.valueOf(TT.getId()));
            parentTask.setTitle(TT.getName());
            parentTask.setDateTime(MainActivity.dateTimeForm.format(cal.getTime()));
            parentTask.setColor(color);

            //Заголовки с датами
            String title = MainActivity.dateForm.format(cal.getTime());

            if(head==null) {
                head = new HeaderItem(title);
            }
            else
            {
                if(!head.getId().equals(title)){
                    head = new HeaderItem(title);
                }
            }

            parentTask.setHeader(head);

            List<TaskTable> childs = DBHelper.getTaskTableFilterByParentId(TT.getId());

            for(TaskTable child: childs) {

                SubItem subTask = new SubItem(String.valueOf(child.getId()));
                subTask.setTitle(child.getName());

                cal.setTimeInMillis(child.getDateMili());
                subTask.setDateTime(MainActivity.dateTimeForm.format(cal.getTime()));

                parentTask.addSubItem(subTask);
            }

            mItems.add(parentTask);
        }
    }

    private List<TaskTable> getTaskTable(){

        List<TaskTable> tasks;
        if (typeId == -1) {
            if(day) {

                mCalendar.set(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                long begin = mCalendar.getTimeInMillis();
                mCalendar.add(Calendar.DAY_OF_MONTH, 1);
                long end = mCalendar.getTimeInMillis() - 1;
                tasks = DBHelper.getTaskTableFilterByParentIdAndTypeDate(typeId, -1, begin, end);
            }
            else
                tasks = DBHelper.getTaskTableFilterByParentId(-1);
        } else {
            if(day) {

                mCalendar.set(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                long begin = mCalendar.getTimeInMillis();
                mCalendar.add(Calendar.DAY_OF_MONTH, 1);
                long end = mCalendar.getTimeInMillis() - 1;
                tasks = DBHelper.getTaskTableFilterByParentIdAndTypeDate(typeId, -1, begin, end);
            }
            else
                tasks = DBHelper.getTaskTableFilterByParentIdAndType(typeId, -1);
        }
        return tasks;
    }


    @Override
    public void onActionStateChanged(RecyclerView.ViewHolder viewHolder, int actionState) {

    }
}
