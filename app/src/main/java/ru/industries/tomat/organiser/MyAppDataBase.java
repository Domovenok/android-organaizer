package ru.industries.tomat.organiser;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by domov on 28.04.2016.
 */

@Database(name = MyAppDataBase.NAME, version = MyAppDataBase.VERSION)
public class MyAppDataBase {

    public static final String NAME = "MyAppDataBase"; // we will add the .db extension

    public static final int VERSION = 1;

}
