package ru.industries.tomat.organiser.TaskList;

import android.support.v7.widget.RecyclerView;

import java.io.Serializable;

import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;

/**
 * This class will benefit of the already implemented methods (getter and setters) in
 * {@link eu.davidea.flexibleadapter.items.AbstractFlexibleItem}.
 *
 * It is used as Base item for all example models.
 */
public abstract class AbstractTaskItem<VH extends RecyclerView.ViewHolder>
        extends AbstractFlexibleItem<VH>
        implements Serializable {

    private static final long serialVersionUID = -6882745111884490060L;

    public String id;
    public String title;
    private String dateTime;

    public AbstractTaskItem(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object inObject) {
        if (inObject instanceof AbstractTaskItem) {
            AbstractTaskItem inItem = (AbstractTaskItem) inObject;
            return this.id.equals(inItem.id);
        }
        return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateTime(){
        return dateTime;
    }

    public void setDateTime(String dateTime){
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", title=" + title;
    }

}